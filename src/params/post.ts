import type { ParamMatcher } from '@sveltejs/kit';
import posts from '$data/posts.json'


export const match = ((param) => {
    return posts.some(post => post.route === param);
}) satisfies ParamMatcher;