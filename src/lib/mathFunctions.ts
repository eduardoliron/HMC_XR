export const pointsDist = (pointA: point, pointB: point) => {
  return Math.sqrt(Math.pow(pointA.x - pointB.x, 2) + Math.pow(pointA.y - pointB.y, 2))
}

export const createPoint = (maxWidth: number, maxHeight: number) => {
  return {
    x: Math.random() * maxWidth,
    y: Math.random() * maxHeight
  }
}

export const createPointInCircle = (center: { x: number, y: number }, radius: number) => {
  const r = radius * Math.sqrt(Math.random())
  const theta = Math.random() * 2 * Math.PI
  return {
    x: center.x + r * Math.cos(theta),
    y: center.y + r * Math.sin(theta)
  }
}

export function getLatLon(vector: { x: number, y: number, z: number }) {
  const lat = getLat(vector.x, vector.y);
  const lon = getLon(vector.x, vector.y, vector.z)
  return { lat, lon };
}

function getLon(x: number, y: number, z: number) {

  const degtorad = Math.PI / 180

  const _x = x * degtorad
  const _y = y * degtorad
  const _z = z * degtorad

  const cX = Math.cos(_x)
  const cY = Math.cos(_y)
  const cZ = Math.cos(_z)
  const sX = Math.sin(_x)
  const sY = Math.sin(_y)
  const sZ = Math.sin(_z)

  // Calculate Vx and Vy components
  const Vx = - cZ * sY - sZ * sX * cY
  const Vy = - sZ * sY + cZ * sX * cY

  // Calculate compass heading
  let compassHeading = Math.atan(Vx / Vy)

  // Convert compass heading to use whole unit circle
  if (Vy < 0) {
    compassHeading += Math.PI
  } else if (Vx < 0) {
    compassHeading += 2 * Math.PI
  }

  return Math.round(compassHeading * (180 / Math.PI) * 100) / 100 // Compass Heading (in degrees)
}

function getLat(x: number, y: number) {
  let pitch = 0

  pitch = x < 0 ?
    x < -90 ? 90 : -90
    : x - 90

  let newGamma = 0
  newGamma = y < 0 ? (90 + y) * -1 : Math.abs(y - 90)

  let correctPitch = 0

  if (!isSafari) {
    if (screen.orientation.type === "portrait-primary") {
      correctPitch = pitch
    } else {
      if (screen.orientation.type === "landscape-secondary") newGamma = -1 * newGamma
      correctPitch = pitch < 0 ?
        newGamma > 0 ? -90 : newGamma
        : newGamma < 0 ? 90 : newGamma
    }
  } else {
    if (window.orientation === 0) {
      correctPitch = pitch
    } else {
      if (window.orientation === -90) newGamma = -1 * newGamma
      correctPitch = pitch < 0 ?
        newGamma > 0 ? -90 : newGamma
        : newGamma < 0 ? 90 : newGamma
    }
  }

  return -Math.round(correctPitch * 100) / 100
}

function isSafari (): boolean {
  return /^((?!chrome|android).)*safari/i.test(navigator.userAgent)
}
