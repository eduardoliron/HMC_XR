import type { PageLoad } from './$types';
import posts from '$data/posts.json'

export const load = (({ params }) => {
    return {
        post: posts.find(post => post.route === params.post)
    };
}) satisfies PageLoad;